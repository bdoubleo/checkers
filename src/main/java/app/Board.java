package app;

import static app.CheckersAppGUI.*;

public class Board {
    public static Tile[][] board = new Tile[WIDTH][HEIGHT];;
    private Board() {
        board = new Tile[WIDTH][HEIGHT];
    }
    public static int coordinateToBoard(double pixel) {
        return (int) ((pixel + TILE_SIZE * 0.5) / TILE_SIZE);
    }
}
