package app;

import javafx.application.Application;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.geometry.Pos;
import javafx.scene.Group;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.stage.Modality;
import javafx.stage.Stage;

import static app.Game.TURN;
import static app.Game.isRunning;


public class CheckersAppGUI extends Application {
    public static final int TILE_SIZE = 80;
    public static final int WIDTH = 8;
    public static final int HEIGHT = 8;

    private final Group tileGroup = new Group();
    public static Group pieceGroup = new Group();
    //public static Stage board;


    private Parent createBoard() {
        Pane root = new Pane();
        root.setPrefSize(WIDTH * TILE_SIZE, HEIGHT * TILE_SIZE);



        root.getChildren().addAll(tileGroup, pieceGroup);
        for (int y = 0; y < HEIGHT; y++) {
            for (int x = 0; x < WIDTH; x++) {
                Tile tile = new Tile((x + y) % 2 == 1, x, y);
                Board.board[x][y] = tile;
                tile.setStroke(javafx.scene.paint.Color.BLACK);

                tileGroup.getChildren().add(tile);

                Piece piece = null;
                if (y <= 2 && (x + y) % 2 != 0) {
                    piece = new Piece(Color.BLACK, x, y);
                }
                if (y >= 5 && (x + y) % 2 != 0) {
                    piece = new Piece(Color.WHITE, x, y);
                }
                if (piece != null) {
                    tile.setPiece(piece);
                    pieceGroup.getChildren().addAll(piece);
                }
            }
        }


        return root;
    }


    @Override
    public void start(Stage primaryStage)/* throws Exception*/ { //ADD TOP MENU
        Image bgMenuImage = new Image("mainBG.png");
        ImageView bgMenu = new ImageView(bgMenuImage);

        VBox mainMenu = new VBox(7);

        Button start = new Button("Start game");
        Button exit = new Button("Quit game");

        mainMenu.setAlignment(Pos.CENTER);
        mainMenu.getChildren().addAll(start, exit);
        StackPane menuPane = new StackPane();
        menuPane.getChildren().addAll(bgMenu, mainMenu);
        Scene primaryScene = new Scene(menuPane);
        primaryStage.setScene(primaryScene);


        primaryStage.setWidth(800);
        primaryStage.setHeight(800);
        primaryStage.setResizable(false);
        primaryStage.setTitle("Checkers");
        primaryStage.show();

        exit.setOnAction(e -> {
            Button source = (Button) e.getSource();
            primaryStage.hide();
        });


        start.setOnAction(e -> {
            Button source = (Button) e.getSource();
            primaryStage.hide();


            Stage board = new Stage();
            Scene scene = new Scene(createBoard());
            board.setScene(scene);
            board.setTitle("ChEcKeRs");
            board.setResizable(false);
            board.show();
            isRunning.setValue(true);
            isRunning.addListener((observable, oldValue, newValue) -> {
                if (!isRunning.getValue()) {
                    endOfGame();
                    board.close();
                }
            });
        });


    }

    public void endOfGame() {
        Stage results = new Stage();

        Image bgMenuImage = new Image("mainBG.png");
        ImageView bgMenu = new ImageView(bgMenuImage);

        VBox endMenu = new VBox(7);

        Button restart = new Button("Restart");
        Button quit = new Button("Quit");

        Label congrats = new Label(TURN.name() + " player has won. Congratulations!");

        endMenu.setAlignment(Pos.CENTER);
        endMenu.getChildren().addAll(congrats, restart, quit);
        StackPane endMenuPane = new StackPane();
        endMenuPane.getChildren().addAll(bgMenu, endMenu);
        Scene endScene = new Scene(endMenuPane);

        results.setScene(endScene);
        results.setAlwaysOnTop(true);
        results.setWidth(400);
        results.setHeight(160);
        results.setResizable(false);
        results.setTitle("Game has ended");

        restart.setOnAction(e -> {
            isRunning.setValue(true);
            results.close();
            start(new Stage());
        });

        quit.setOnAction(e -> {
            Button source = (Button) e.getSource();
            results.close();

        });

        results.initModality(Modality.APPLICATION_MODAL);
        results.showAndWait();

        //results.show();


    }


    public static void main(String[] args) {
        launch(args);
    }
}
