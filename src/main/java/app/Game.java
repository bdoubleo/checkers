package app;

import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;

public class Game  {
    public static Color TURN = Color.BLACK;
    public static BooleanProperty isRunning = new SimpleBooleanProperty();


    public static void changeTurns() {
        if (TURN.equals(Color.WHITE)) TURN = Color.BLACK;
        else TURN = Color.WHITE;
    }

}
