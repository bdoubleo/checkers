package app;

import javafx.beans.Observable;

public enum Color {
    BLACK(1), WHITE(-1);

    final int moveDirection;


    Color(int moveDirection) {
        this.moveDirection = moveDirection;
    }

    @Override
    public String toString() {
        if (moveDirection == 1) {
            return ("Black");
        } else return ("White");
    }
}
