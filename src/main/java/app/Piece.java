package app;

import javafx.scene.layout.StackPane;
import javafx.scene.shape.Ellipse;

import java.util.ArrayList;
import java.util.List;

import static app.Board.board;
import static app.Board.coordinateToBoard;
import static app.CheckersAppGUI.*;
import static app.Game.TURN;
import static java.lang.Math.floor;


public class Piece extends StackPane {
    public final Color pieceColor;


    public Color getColor() {
        return pieceColor;
    }

    public double oldX, oldY;
    public double mouseX, mouseY;

    public double getOldX() {
        return oldX;
    }

    public double getOldY() {
        return oldY;
    }

    public Piece(Color pieceColor, int x, int y) {
        this.pieceColor = pieceColor;
        move(x, y);
        Ellipse bg = new Ellipse(TILE_SIZE * 0.3, TILE_SIZE * 0.25);
        if (pieceColor == Color.BLACK) {
            bg.setFill(javafx.scene.paint.Color.BLACK);
        } else {
            bg.setFill(javafx.scene.paint.Color.WHITE);
            bg.setStroke(javafx.scene.paint.Color.BLACK);
            bg.setStrokeWidth(TILE_SIZE * 0.015);
        }
        bg.setTranslateX((TILE_SIZE - TILE_SIZE * 0.3 * 2) / 2);
        bg.setTranslateY(((TILE_SIZE - TILE_SIZE * 0.25 * 2) / 2));
        getChildren().addAll(bg);
        setOnMousePressed(e -> {
            mouseX = e.getSceneX();
            mouseY = e.getSceneY();

        });
        setOnMouseDragged(e -> {
            relocate(e.getSceneX() - mouseX + oldX, e.getSceneY() - mouseY + oldY);
        });
        setOnMouseReleased(e -> {
            int newX = coordinateToBoard(this.getLayoutX());
            int newY = coordinateToBoard(this.getLayoutY());
            MoveResult result;
            if (newX < 0 || newY < 0 || newX >= WIDTH || newY >= HEIGHT) {
                result = new MoveResult(MoveType.NONE);
            } else {
                result = tryToMove(this, newX, newY);
            }

            int x0 = coordinateToBoard(oldX);
            int y0 = coordinateToBoard(oldY);

            switch (result.getType()) {
                case NONE:
                    if (Game.isRunning.getValue()) {
                        this.abortMove();
                        break;
                    }
                case MOVE:
                    if (Game.isRunning.getValue()) {
                        board[x0][y0].setPiece(null);
                        board[newX][newY].setPiece(this);

                        this.move(newX, newY);
                        if ((this.getColor() == Color.BLACK && newY == 7) ||
                                (this.getColor() == Color.WHITE && newY == 0)) {
                            Piece piece = new King(this.getColor(), newX, newY);
                            board[newX][newY].setPiece(piece);
                            pieceGroup.getChildren().remove(this);
                            pieceGroup.getChildren().add(piece);
                        }
                        Game.changeTurns();
                    }

                    break;
                case JUMP:
                    if (Game.isRunning.getValue()) {
                        this.move(newX, newY);
                        board[x0][y0].setPiece(null);
                        board[newX][newY].setPiece(this);

                        if ((this.getColor() == Color.BLACK && newY == 7) ||
                                (this.getColor() == Color.WHITE && newY == 0)) {
                            Piece king = new King(this.getColor(), newX, newY);
                            board[newX][newY].setPiece(king);
                            pieceGroup.getChildren().remove(this);
                            pieceGroup.getChildren().add(king);
                            //Game.changeTurns();
                        }

                        Piece killedPiece = result.getPiece();
                        board[coordinateToBoard(killedPiece.getOldX())][coordinateToBoard(killedPiece.getOldY())].setPiece(null);
                        CheckersAppGUI.pieceGroup.getChildren().remove(killedPiece);
                        if (!canJump(board[newX][newY].getPiece())) {
                            Game.changeTurns();
                            if (isFinished()) {
                                Game.changeTurns();
                                Game.isRunning.setValue(false);
                            }

                        }

                        break;
                    }
            }
        });
    }

    public boolean isFinished() {
        for (int i = 0; i < 8; i++) {
            for (int j = 0; j < 8; j++) {
                if (board[i][j].hasPiece()) {
                    if (board[i][j].getPiece().getColor().equals(TURN)) {
                        return !canMove(board[i][j].getPiece());
                    }
                }
            }
        }
        return true;
    }

    public MoveResult tryToMove(Piece piece, int newX, int newY) {
        if (board[newX][newY].hasPiece() || (newX + newY) % 2 == 0) {
            return new MoveResult(MoveType.NONE);
        }
        int x0 = coordinateToBoard(piece.getOldX());
        int y0 = coordinateToBoard(piece.getOldY());


        if (!jumpsAvailable(TURN) && Math.abs(newX - x0) == 1
                && newY - y0 == piece.getColor().moveDirection && TURN.equals(piece.getColor())) {

            //Game.changeTurns();
            return new MoveResult(MoveType.MOVE);
        }
        if (jumpsAvailable(TURN)) {
            int x1 = (int) (x0 + floor((double) (newX - x0) / 2));
            int y1 = (int) (y0 + floor((double) (newY - y0) / 2));

            if (jumpsAvailable(TURN) && TURN.equals(piece.getColor()) && board[x1][y1].hasPiece() &&
                    /*newY - y0 == (piece.getColor().moveDirection * 2) && */board[x1][y1].getPiece().getColor() != piece.getColor()
            ) {


                return new MoveResult(MoveType.JUMP, board[x1][y1].getPiece());

            }
        }
        return new MoveResult(MoveType.NONE);
    }

    public void move(int x, int y) {
        oldX = x * TILE_SIZE;
        oldY = y * TILE_SIZE;
        relocate(oldX, oldY);
    }


    public void abortMove() {
        relocate(oldX, oldY);
    }


    public boolean jumpsAvailable(Color strType) {
        for (int i = 0; i < 8; i++) {
            for (int j = 0; j < 8; j++) {
                if (board[i][j].hasPiece()) {
                    if (canJump(board[i][j].getPiece()) &&
                            board[i][j].getPiece().getColor().equals(strType))
                        return true;
                }
            }
        }
        return false;
    }


    public boolean canJump(Piece piece) {
        List<Integer> result = new ArrayList<>();
        int[] directions = new int[]{-1, 1, -1, -1, 1, 1, 1, -1};

        int x = coordinateToBoard(piece.getOldX());
        int y = coordinateToBoard(piece.getOldY());
        for (int j = 1; j < 8; j += 2) {
            if (x + 2 * directions[j - 1] > -1 && x + 2 * directions[j - 1] < 8
                    && y + 2 * directions[j] > -1 && y + 2 * directions[j] < 8) {

                int newX = directions[j - 1];
                int newY = directions[j];
                if (board[x + newX][y + newY].hasPiece()) {

                    if (board[x + newX][y + newY].getPiece().getColor() != piece.getColor()
                            && !board[x + 2 * newX][y + 2 * newY].hasPiece()) {

                        return true;

                    }
                }
            }
        }

        return false;

    }

    public boolean canMove(Piece piece) {
        int[] directions = new int[]{-1, 1, 1, 1, -1, -1, 1, -1};
        /*-1,1,-1,-1,
        1,1,1,-1*/
        int x = coordinateToBoard(piece.getOldX());
        int y = coordinateToBoard(piece.getOldY());
        if (piece.getColor().equals(Color.BLACK)) {
            for (int j = 1; j < 4; j += 2) {
                if (x + 2 * directions[j - 1] > -1 && x + 2 * directions[j - 1] < 8
                        && y + 2 * directions[j] > -1 && y + 2 * directions[j] < 8) {

                    int newX = x + directions[j - 1];
                    int newY = y + directions[j];
                    if (!tryToMove(piece, newX, newY).equals(new MoveResult(MoveType.NONE))) {
                        return true;
                    }
                }
            }
        } else {
            for (int j = 5; j < 8; j += 2) {
                if (x + 2 * directions[j - 1] > -1 && x + 2 * directions[j - 1] < 8
                        && y + 2 * directions[j] > -1 && y + 2 * directions[j] < 8) {

                    int newX = x + directions[j - 1];
                    int newY = y + directions[j];
                    if (!tryToMove(piece, newX, newY).equals(new MoveResult(MoveType.NONE))) {
                        return true;
                    }
                }
            }
        }
        return false;

    }
}