package app;


import javafx.scene.shape.Ellipse;

import java.util.ArrayList;
import java.util.List;

import static app.Board.board;
import static app.Board.coordinateToBoard;
import static app.CheckersAppGUI.TILE_SIZE;
import static app.Game.TURN;
import static java.lang.Math.floor;

public class King extends Piece {  //Наследуется от Piece и переопределяет условия дивжения/взятия
    public King(Color type, int x, int y) {
        super(type, x, y);
        Ellipse RED = new Ellipse(TILE_SIZE * 0.1, TILE_SIZE * 0.07);
        RED.setFill(javafx.scene.paint.Color.RED);
        RED.setTranslateX((TILE_SIZE - TILE_SIZE * 0.3 * 2) / 2);
        RED.setTranslateY(((TILE_SIZE - TILE_SIZE * 0.25 * 2) / 2));
        getChildren().addAll(RED);
    }

    @Override
    public MoveResult tryToMove(Piece piece, int newX, int newY) {
        if (board[newX][newY].hasPiece() || (newX + newY) % 2 == 0) {
            return new MoveResult(MoveType.NONE);
        }
        int x0 = coordinateToBoard(piece.getOldX());
        int y0 = coordinateToBoard(piece.getOldY());


        if (!jumpsAvailable(TURN) && Math.abs(newX - x0) == 1
                && TURN.equals(piece.getColor())) {

            //Game.changeTurns();
            return new MoveResult(MoveType.MOVE);
        }
        if (jumpsAvailable(TURN)) {
            int x1 = (int) (x0 + floor((double) (newX - x0) / 2));
            int y1 = (int) (y0 + floor((double) (newY - y0) / 2));

            if (jumpsAvailable(TURN) && TURN.equals(piece.getColor()) && board[x1][y1].hasPiece() &&
                    board[x1][y1].getPiece().getColor() != piece.getColor()
                    && TURN.equals(piece.getColor())) {


                return new MoveResult(MoveType.JUMP, board[x1][y1].getPiece());

            }
        }
        return new MoveResult(MoveType.NONE);
    }
    /*public void move(int x, int y) {
        oldX = x * TILE_SIZE;
        oldY = y * TILE_SIZE;
        relocate(oldX, oldY);
    }*/


    /*public void abortMove() {
        relocate(oldX, oldY);
    }*/


    /*public boolean jumpsAvailable(Color strType) {
        for (int i = 0; i < 8; i++) {
            for (int j = 0; j < 8; j++) {
                if (board[i][j].hasPiece()) {
                    if (!canJump(board[i][j].getPiece()).isEmpty() &&
                            board[i][j].getPiece().getColor().equals(strType))
                        return true;
                }
            }
        }
        return false;
    }*/


    /*public boolean canJump(Piece piece) {
        List<Integer> result = new ArrayList<>();
        int[] directions = new int[]{-1, 1, -1, -1, 1, 1, 1, -1};

        int x = coordinateToBoard(piece.getOldX());
        int y = coordinateToBoard(piece.getOldY());
        for (int j = 1; j < 8; j += 2) {
            if (x + 2 * directions[j - 1] > -1 && x + 2 * directions[j - 1] < 8
                    && y + 2 * directions[j] > -1 && y + 2 * directions[j] < 8) {

                int newX = directions[j - 1];
                int newY = directions[j];
                if (board[x + newX][y + newY].hasPiece()) {

                    if (board[x + newX][y + newY].getPiece().getColor() != piece.getColor()
                            && !board[x + 2 * newX][y + 2 * newY].hasPiece()) {

                        return true;

                    }
                }
            }
        }

        return false;

    }*/

    public boolean canMove(Piece piece) {
        int[] directions = new int[]{-1, 1, 1, 1, -1, -1, 1, -1};
        /*-1,1,-1,-1,
        1,1,1,-1*/
        int x = coordinateToBoard(piece.getOldX());
        int y = coordinateToBoard(piece.getOldY());

        for (int j = 1; j < 8; j += 2) {
            if (x + 2 * directions[j - 1] > -1 && x + 2 * directions[j - 1] < 8
                    && y + 2 * directions[j] > -1 && y + 2 * directions[j] < 8) {

                int newX = x + directions[j - 1];
                int newY = y + directions[j];
                if (!tryToMove(piece, newX, newY).equals(new MoveResult(MoveType.NONE))) {
                    return true;
                }
            }
        }
        return false;
    }
}


